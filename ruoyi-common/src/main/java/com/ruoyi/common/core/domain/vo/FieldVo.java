package com.ruoyi.common.core.domain.vo;

/**
 * Description: TODO
 *
 * @Author: WANGWENFANG
 * @Date: 2021/11/2 14:15
 */
public class FieldVo {

    private String prop;

    private String propName;

    public FieldVo(String prop, String propName) {
        this.prop = prop;
        this.propName = propName;
    }

    public String getProp() {
        return prop;
    }

    public void setProp(String prop) {
        this.prop = prop;
    }

    public String getPropName() {
        return propName;
    }

    public void setPropName(String propName) {
        this.propName = propName;
    }

}
