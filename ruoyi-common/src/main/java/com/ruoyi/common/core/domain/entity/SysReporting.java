package com.ruoyi.common.core.domain.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * 报对象 sys_reporting
 *
 * @author ruoyi
 * @date 2021-10-20
 */
public class SysReporting extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 报表id */
    private Long reportingId;

    /** 父报表id */
    @Excel(name = "父报表id")
    private Long parentId;

    /** 级别（C类别 R报表） */
    @Excel(name = "级别标志", readConverterExp = "C=类别,R=报表")
    private String levelCode;

    /** 报表编号 */
    @Excel(name = "报表编号")
    private String reportingCode;

    /** 报表名称 */
    @Excel(name = "报表名称")
    private String reportingName;

    /** 显示顺序 */
    @Excel(name = "显示顺序")
    private Integer orderNum;

    /** 报表状态（0正常 1停用） */
    @Excel(name = "报表状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    private List<SysReporting> children = new ArrayList<SysReporting>();

    public void setReportingId(Long reportingId)
    {
        this.reportingId = reportingId;
    }

    public Long getReportingId()
    {
        return reportingId;
    }
    public void setParentId(Long parentId)
    {
        this.parentId = parentId;
    }

    public Long getParentId()
    {
        return parentId;
    }
    public void setLevelCode(String levelCode)
    {
        this.levelCode = levelCode;
    }

    public String getLevelCode()
    {
        return levelCode;
    }
    public void setReportingCode(String reportingCode)
    {
        this.reportingCode = reportingCode;
    }

    public String getReportingCode()
    {
        return reportingCode;
    }
    public void setReportingName(String reportingName)
    {
        this.reportingName = reportingName;
    }

    public String getReportingName()
    {
        return reportingName;
    }
    public void setOrderNum(Integer orderNum)
    {
        this.orderNum = orderNum;
    }

    public Integer getOrderNum()
    {
        return orderNum;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    public List<SysReporting> getChildren() {
        return children;
    }

    public void setChildren(List<SysReporting> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("reportingId", getReportingId())
                .append("parentId", getParentId())
                .append("levelCode", getLevelCode())
                .append("reportingCode", getReportingCode())
                .append("reportingName", getReportingName())
                .append("remark", getRemark())
                .append("orderNum", getOrderNum())
                .append("status", getStatus())
                .append("delFlag", getDelFlag())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}
