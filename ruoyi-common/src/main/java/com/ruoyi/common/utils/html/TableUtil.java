package com.ruoyi.common.utils.html;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.core.domain.vo.FieldVo;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.Excels;
import com.ruoyi.common.utils.bean.ReportBaseEntity;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Description: TODO
 *
 * @Author: WANGWENFANG
 * @Date: 2021/11/2 14:03
 */
//public class TableUtil<T extends BaseEntity> {
public class TableUtil<T> {

    /**
     * 实体对象
     */
    public Class<T> clazz;

    public TableUtil(Class<T> clazz)
    {
        this.clazz = clazz;
    }

    /**
     * 获取表头项目名称集合
     */
    public List<FieldVo> getFieldVos()
    {
        List<FieldVo> fieldVos = new ArrayList<FieldVo>();
        List<Field> tempFields = new ArrayList<>();
        tempFields.addAll(Arrays.asList(clazz.getSuperclass().getDeclaredFields()));
        tempFields.addAll(Arrays.asList(clazz.getDeclaredFields()));
        for (Field field : tempFields)
        {
            // 单注解
            if (field.isAnnotationPresent(Excel.class))
            {
                Excel attr = field.getAnnotation(Excel.class);
                if (attr != null && (attr.type() == Excel.Type.ALL || attr.type() == Excel.Type.IMPORT))
                {
                    field.setAccessible(true);
                    fieldVos.add(new FieldVo(field.getName(),attr.name()));
                }
            }

            // 多注解
            if (field.isAnnotationPresent(Excels.class))
            {
                Excels attrs = field.getAnnotation(Excels.class);
                Excel[] excels = attrs.value();
                for (Excel attr : excels)
                {
                    if (attr != null && (attr.type() == Excel.Type.ALL || attr.type() == Excel.Type.IMPORT))
                    {
                        field.setAccessible(true);
                        fieldVos.add(new FieldVo(field.getName(),attr.name()));
                    }
                }
            }
        }
        return fieldVos;
    }

}
