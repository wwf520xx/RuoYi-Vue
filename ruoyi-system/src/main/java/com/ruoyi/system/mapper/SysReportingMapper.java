package com.ruoyi.system.mapper;

import java.util.List;

import com.ruoyi.common.core.domain.entity.SysReporting;
import org.apache.ibatis.annotations.Param;

/**
 * 报Mapper接口
 * 
 * @author ruoyi
 * @date 2021-10-20
 */
public interface SysReportingMapper 
{

    /**
     * 查询报表
     * 
     * @param reportingId 报主键
     * @return 报
     */
    public SysReporting selectReportingByReportingId(Long reportingId);

    /**
     * 查询报列表
     * 
     * @param reporting 报
     * @return 报集合
     */
    public List<SysReporting> selectReportingList(SysReporting reporting);

    /**
     * 新增报表
     * 
     * @param reporting 报表
     * @return 结果
     */
    public int insertReporting(SysReporting reporting);

    /**
     * 修改报
     * 
     * @param sysReporting 报
     * @return 结果
     */
    public int updateReporting(SysReporting sysReporting);

    /**
     * 删除报
     * 
     * @param reportingId 报主键
     * @return 结果
     */
    public int deleteReportingByReportingId(Long reportingId);

    /**
     * 批量删除报
     * 
     * @param reportingIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteReportingByReportingIds(Long[] reportingIds);

    /**
     * 根据用户查询系统报表列表
     *
     * @param reporting 菜单信息
     * @return 菜单列表
     */
    public List<SysReporting> selectReportingListByUserId(SysReporting reporting);

    /**
     * 校验报表名称是否唯一
     *
     * @param reportingName 报表名称
     * @param parentId 父报表ID
     * @return 结果
     */
    public SysReporting checkReportingNameUnique(@Param("reportingName") String reportingName, @Param("parentId") Long parentId);

    /**
     * 校验报表编号是否唯一
     *
     * @param reportingCode 报表编号
     * @return 结果
     */
    public SysReporting checkReportingCodeUnique(@Param("reportingCode") String reportingCode);

    /**
     * 是否存在报表子节点
     *
     * @param reportingId 报表ID
     * @return 结果
     */
    public int hasChildByReportingId(Long reportingId);

    /**
     * 根据角色ID查询表单树信息
     *
     * @param roleId 角色ID
     * @param reportingCheckStrictly 表单树选择项是否关联显示
     * @return 选中表单列表
     */
    public List<Integer> selectReportingListByRoleId(@Param("roleId") Long roleId, @Param("reportingCheckStrictly") boolean reportingCheckStrictly);

    /**
     * 查询报表使用数量
     *
     * @param reportingId 报表ID
     * @return 结果
     */
    public int checkReportingExistRole(Long reportingId);
}
