package com.ruoyi.system.mapper;

//import com.ruoyi.system.domain.ReportingTconfirm;

//import com.hsbcjt.reporting.domain.entity.ReportingTconfirm;

import com.ruoyi.system.domain.reporting.ReportingTconfirm;
import com.ruoyi.system.domain.vo.RequestReportParamsVo;

import java.util.List;

/**
 * Description: TODO
 *
 * @Author: WANGWENFANG
 * @Date: 2021/10/26 13:43
 */
public interface ReportingTconfirmMapper {

    public List<ReportingTconfirm> selectReportingTconfirmList(RequestReportParamsVo requestReportParamsVo);

}
