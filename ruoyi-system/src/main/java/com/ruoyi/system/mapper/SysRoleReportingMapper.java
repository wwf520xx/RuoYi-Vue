package com.ruoyi.system.mapper;

import java.util.List;

import com.ruoyi.system.domain.SysRoleDept;
import com.ruoyi.system.domain.SysRoleReporting;

/**
 * 角色和报关联Mapper接口
 * 
 * @author ruoyi
 * @date 2021-10-21
 */
public interface SysRoleReportingMapper 
{

    /**
     * 查询角色和报关联
     * 
     * @param roleId 角色和报关联主键
     * @return 角色和报关联
     */
    public SysRoleReporting selectSysRoleReportingByRoleId(Long roleId);

    /**
     * 查询角色和报关联列表
     * 
     * @param sysRoleReporting 角色和报关联
     * @return 角色和报关联集合
     */
    public List<SysRoleReporting> selectSysRoleReportingList(SysRoleReporting sysRoleReporting);

    /**
     * 新增角色和报表关联
     * 
     * @param sysRoleReporting 角色和报关联
     * @return 结果
     */
    public int insertSysRoleReporting(SysRoleReporting sysRoleReporting);

    /**
     * 修改角色和报关联
     * 
     * @param sysRoleReporting 角色和报关联
     * @return 结果
     */
    public int updateSysRoleReporting(SysRoleReporting sysRoleReporting);

    /**
     * 删除角色和报关联
     * 
     * @param roleId 角色和报关联主键
     * @return 结果
     */
    public int deleteSysRoleReportingByRoleId(Long roleId);

    /**
     * 批量删除角色和报关联
     * 
     * @param roleIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysRoleReportingByRoleIds(Long[] roleIds);

    /**
     * 查询报表使用数量
     *
     * @param reportingId 报表ID
     * @return 结果
     */
    public int checkReportingExistRole(Long reportingId);

    /**
     * 通过角色ID删除角色和报表关联
     *
     * @param roleId 角色ID
     * @return 结果
     */
    public int deleteRoleReportingByRoleId(Long roleId);

    /**
     * 批量新增角色部门信息
     *
     * @param roleReporting 角色部门列表
     * @return 结果
     */
    public int batchRoleReporting(SysRoleReporting roleReporting);

}
