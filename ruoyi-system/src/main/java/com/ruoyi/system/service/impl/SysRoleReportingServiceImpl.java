package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysRoleReportingMapper;
import com.ruoyi.system.domain.SysRoleReporting;
import com.ruoyi.system.service.ISysRoleReportingService;

/**
 * 角色和报关联Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-10-21
 */
@Service
public class SysRoleReportingServiceImpl implements ISysRoleReportingService 
{
    @Autowired
    private SysRoleReportingMapper sysRoleReportingMapper;

    /**
     * 查询角色和报关联
     * 
     * @param roleId 角色和报关联主键
     * @return 角色和报关联
     */
    @Override
    public SysRoleReporting selectSysRoleReportingByRoleId(Long roleId)
    {
        return sysRoleReportingMapper.selectSysRoleReportingByRoleId(roleId);
    }

    /**
     * 查询角色和报关联列表
     * 
     * @param sysRoleReporting 角色和报关联
     * @return 角色和报关联
     */
    @Override
    public List<SysRoleReporting> selectSysRoleReportingList(SysRoleReporting sysRoleReporting)
    {
        return sysRoleReportingMapper.selectSysRoleReportingList(sysRoleReporting);
    }

    /**
     * 新增角色和报关联
     * 
     * @param sysRoleReporting 角色和报关联
     * @return 结果
     */
    @Override
    public int insertSysRoleReporting(SysRoleReporting sysRoleReporting)
    {
        return sysRoleReportingMapper.insertSysRoleReporting(sysRoleReporting);
    }

    /**
     * 修改角色和报关联
     * 
     * @param sysRoleReporting 角色和报关联
     * @return 结果
     */
    @Override
    public int updateSysRoleReporting(SysRoleReporting sysRoleReporting)
    {
        return sysRoleReportingMapper.updateSysRoleReporting(sysRoleReporting);
    }

    /**
     * 批量删除角色和报关联
     * 
     * @param roleIds 需要删除的角色和报关联主键
     * @return 结果
     */
    @Override
    public int deleteSysRoleReportingByRoleIds(Long[] roleIds)
    {
        return sysRoleReportingMapper.deleteSysRoleReportingByRoleIds(roleIds);
    }

    /**
     * 删除角色和报关联信息
     * 
     * @param roleId 角色和报关联主键
     * @return 结果
     */
    @Override
    public int deleteSysRoleReportingByRoleId(Long roleId)
    {
        return sysRoleReportingMapper.deleteSysRoleReportingByRoleId(roleId);
    }
}
