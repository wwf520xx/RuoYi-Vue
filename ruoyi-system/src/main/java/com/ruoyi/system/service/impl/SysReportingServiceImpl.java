package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.TreeSelect;
import com.ruoyi.common.core.domain.entity.SysReporting;
import com.ruoyi.common.core.domain.entity.SysRole;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.mapper.SysRoleMapper;
import com.ruoyi.system.mapper.SysRoleReportingMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysReportingMapper;
import com.ruoyi.system.service.ISysReportingService;

/**
 * 报Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-10-20
 */
@Service
public class SysReportingServiceImpl implements ISysReportingService 
{
    @Autowired
    private SysReportingMapper reportingMapper;

    @Autowired
    private SysRoleReportingMapper roleReportingMapper;

    @Autowired
    private SysRoleMapper roleMapper;

    /**
     * 查询报
     * 
     * @param reportingId 报主键
     * @return 报
     */
    @Override
    public SysReporting selectReportingByReportingId(Long reportingId)
    {
        return reportingMapper.selectReportingByReportingId(reportingId);
    }

    @Override
    public List<SysReporting> selectReportingList(Long userId) {
        return selectReportingList(new SysReporting(), userId);
    }

    /**
     * 查询报列表
     * 
     * @param reporting 报
     * @param userId 报
     * @return 报
     */
    @Override
    public List<SysReporting> selectReportingList(SysReporting reporting, Long userId)
    {
        List<SysReporting> reportingList = null;
        // 管理员显示所有报表信息
        if (SysUser.isAdmin(userId))
        {
            reportingList = reportingMapper.selectReportingList(reporting);
        }
        else
        {
            reporting.getParams().put("userId", userId);
            reportingList = reportingMapper.selectReportingListByUserId(reporting);
        }
        return reportingList;
    }

    /**
     * 新增报
     * 
     * @param reporting 报
     * @return 结果
     */
    @Override
    public int insertReporting(SysReporting reporting)
    {
        reporting.setCreateTime(DateUtils.getNowDate());
        return reportingMapper.insertReporting(reporting);
    }

    /**
     * 修改报
     * 
     * @param sysReporting 报
     * @return 结果
     */
    @Override
    public int updateReporting(SysReporting sysReporting)
    {
        sysReporting.setUpdateTime(DateUtils.getNowDate());
        return reportingMapper.updateReporting(sysReporting);
    }

    /**
     * 批量删除报
     * 
     * @param reportingIds 需要删除的报主键
     * @return 结果
     */
    @Override
    public int deleteReportingByReportingIds(Long[] reportingIds)
    {
        return reportingMapper.deleteReportingByReportingIds(reportingIds);
    }

    /**
     * 删除报信息
     * 
     * @param reportingId 报主键
     * @return 结果
     */
    @Override
    public int deleteReportingByReportingId(Long reportingId)
    {
        return reportingMapper.deleteReportingByReportingId(reportingId);
    }

    /**
     * 校验报表名称是否唯一
     *
     * @param reporting 菜单信息
     * @return 结果
     */
    @Override
    public String checkReportingNameUnique(SysReporting reporting) {
        Long reportingId = StringUtils.isNull(reporting.getReportingId()) ? -1L : reporting.getReportingId();
        SysReporting info = reportingMapper.checkReportingNameUnique(reporting.getReportingName(), reporting.getParentId());
        if (StringUtils.isNotNull(info) && info.getReportingId().longValue() != reportingId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    @Override
    public String checkReportingCodeUnique(SysReporting reporting) {
        Long reportingId = StringUtils.isNull(reporting.getReportingId()) ? -1L : reporting.getReportingId();
        SysReporting info = reportingMapper.checkReportingCodeUnique(reporting.getReportingCode());
        if (StringUtils.isNotNull(info) && info.getReportingId().longValue() != reportingId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 是否存在报表子节点
     *
     * @param reportingId 报表ID
     * @return 结果
     */
    @Override
    public boolean hasChildByReportingId(Long reportingId) {
        int result = reportingMapper.hasChildByReportingId(reportingId);
        return result > 0 ? true : false;
    }

    /**
     * 查询报表使用数量
     *
     * @param reportingId 报表ID
     * @return 结果
     */
    @Override
    public boolean checkReportingExistRole(Long reportingId) {
        int result = roleReportingMapper.checkReportingExistRole(reportingId);
        return result > 0 ? true : false;
    }

    @Override
    public List<TreeSelect> buildReportingTreeSelect(List<SysReporting> reportings) {
        List<SysReporting> reportingTrees = buildReportingTree(reportings);
        return reportingTrees.stream().map(TreeSelect::new).collect(Collectors.toList());
    }

    @Override
    public List<SysReporting> buildReportingTree(List<SysReporting> reportings) {
        List<SysReporting> returnList = new ArrayList<SysReporting>();
        List<Long> tempList = new ArrayList<Long>();
        for (SysReporting reporting : reportings)
        {
            tempList.add(reporting.getReportingId());
        }
        for (Iterator<SysReporting> iterator = reportings.iterator(); iterator.hasNext();)
        {
            SysReporting reporting = (SysReporting) iterator.next();
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(reporting.getParentId()))
            {
                recursionFn(reportings, reporting);
                returnList.add(reporting);
            }
        }
        if (returnList.isEmpty())
        {
            returnList = reportings;
        }
        return returnList;
    }

    @Override
    public List<Integer> selectReportingListByRoleId(Long roleId) {
        SysRole role = roleMapper.selectRoleById(roleId);
        return reportingMapper.selectReportingListByRoleId(roleId, role.isReportingCheckStrictly());
    }

    /**
     * 递归列表
     *
     * @param list
     * @param t
     */
    private void recursionFn(List<SysReporting> list, SysReporting t)
    {
        // 得到子节点列表
        List<SysReporting> childList = getChildList(list, t);
        t.setChildren(childList);
        for (SysReporting tChild : childList)
        {
            if (hasChild(list, tChild))
            {
                recursionFn(list, tChild);
            }
        }
    }

    private List<SysReporting> getChildList(List<SysReporting> list, SysReporting t)
    {
        List<SysReporting> tlist = new ArrayList<SysReporting>();
        Iterator<SysReporting> it = list.iterator();
        while (it.hasNext())
        {
            SysReporting n = (SysReporting) it.next();
            if (n.getParentId().longValue() == t.getReportingId().longValue())
            {
                tlist.add(n);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<SysReporting> list, SysReporting t)
    {
        return getChildList(list, t).size() > 0 ? true : false;
    }

}
