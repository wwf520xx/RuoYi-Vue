package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.common.core.domain.TreeSelect;
import com.ruoyi.common.core.domain.entity.SysReporting;

/**
 * 报Service接口
 * 
 * @author ruoyi
 * @date 2021-10-20
 */
public interface ISysReportingService 
{
    /**
     * 查询报
     * 
     * @param reportingId 报主键
     * @return 报
     */
    public SysReporting selectReportingByReportingId(Long reportingId);

    /**
     * 根据用户查询系统报表列表
     *
     * @param userId 用户ID
     * @return 报表列表
     */
    public List<SysReporting> selectReportingList(Long userId);

    /**
     * 查询报列表
     * 
     * @param reporting 报
     * @return 报集合
     */
    public List<SysReporting> selectReportingList(SysReporting reporting, Long userId);

    /**
     * 新增报表
     * 
     * @param reporting 报表
     * @return 结果
     */
    public int insertReporting(SysReporting reporting);

    /**
     * 修改报
     * 
     * @param sysReporting 报
     * @return 结果
     */
    public int updateReporting(SysReporting sysReporting);

    /**
     * 批量删除报
     * 
     * @param reportingIds 需要删除的报主键集合
     * @return 结果
     */
    public int deleteReportingByReportingIds(Long[] reportingIds);

    /**
     * 删除报信息
     * 
     * @param reportingId 报主键
     * @return 结果
     */
    public int deleteReportingByReportingId(Long reportingId);

    /**
     * 校验报表名称是否唯一
     *
     * @param reporting 报表信息
     * @return 结果
     */
    public String checkReportingNameUnique(SysReporting reporting);

    /**
     * 校验报表编码是否唯一
     *
     * @param reporting 报表信息
     * @return 结果
     */
    public String checkReportingCodeUnique(SysReporting reporting);

    /**
     * 是否存在报表子节点
     *
     * @param reportingId 报表ID
     * @return 结果 true 存在 false 不存在
     */
    public boolean hasChildByReportingId(Long reportingId);

    /**
     * 查询报表是否存在角色
     *
     * @param reportingId 报表ID
     * @return 结果 true 存在 false 不存在
     */
    public boolean checkReportingExistRole(Long reportingId);

    /**
     * 构建前端所需要下拉树结构
     *
     * @param reportings 菜单列表
     * @return 下拉树结构列表
     */
    public List<TreeSelect> buildReportingTreeSelect(List<SysReporting> reportings);

    /**
     * 构建前端所需要树结构
     *
     * @param reportings 菜单列表
     * @return 树结构列表
     */
    public List<SysReporting> buildReportingTree(List<SysReporting> reportings);

    /**
     * 根据角色ID查询报表树信息
     *
     * @param roleId 角色ID
     * @return 选中报表列表
     */
    public List<Integer> selectReportingListByRoleId(Long roleId);

}
