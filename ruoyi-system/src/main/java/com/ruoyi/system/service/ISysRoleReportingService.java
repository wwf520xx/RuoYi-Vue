package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.SysRoleReporting;

/**
 * 角色和报关联Service接口
 * 
 * @author ruoyi
 * @date 2021-10-21
 */
public interface ISysRoleReportingService 
{
    /**
     * 查询角色和报关联
     * 
     * @param roleId 角色和报关联主键
     * @return 角色和报关联
     */
    public SysRoleReporting selectSysRoleReportingByRoleId(Long roleId);

    /**
     * 查询角色和报关联列表
     * 
     * @param sysRoleReporting 角色和报关联
     * @return 角色和报关联集合
     */
    public List<SysRoleReporting> selectSysRoleReportingList(SysRoleReporting sysRoleReporting);

    /**
     * 新增角色和报关联
     * 
     * @param sysRoleReporting 角色和报关联
     * @return 结果
     */
    public int insertSysRoleReporting(SysRoleReporting sysRoleReporting);

    /**
     * 修改角色和报关联
     * 
     * @param sysRoleReporting 角色和报关联
     * @return 结果
     */
    public int updateSysRoleReporting(SysRoleReporting sysRoleReporting);

    /**
     * 批量删除角色和报关联
     * 
     * @param roleIds 需要删除的角色和报关联主键集合
     * @return 结果
     */
    public int deleteSysRoleReportingByRoleIds(Long[] roleIds);

    /**
     * 删除角色和报关联信息
     * 
     * @param roleId 角色和报关联主键
     * @return 结果
     */
    public int deleteSysRoleReportingByRoleId(Long roleId);
}
