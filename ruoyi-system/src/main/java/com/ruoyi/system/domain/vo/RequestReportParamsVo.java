package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.system.mapper.ReportingTconfirmMapper;

/**
 * Description: TODO
 *
 * @Author: WANGWENFANG
 * @Date: 2021/11/3 10:56
 */
public class RequestReportParamsVo {

    private ReportingTconfirmMapper reportingTconfirmMapper;

    public ReportingTconfirmMapper getReportingTconfirmMapper() {
        return reportingTconfirmMapper;
    }

    public void setReportingTconfirmMapper(ReportingTconfirmMapper reportingTconfirmMapper) {
        this.reportingTconfirmMapper = reportingTconfirmMapper;
    }

    private String tableName;

    private String C_CUSTNO;

    private String C_BUSINFLAG;

    private String D_CDATE;

    private String C_CSERIALNO;

    private String D_REQUESTDATE;

    private String D_REQUESTTIME;

    private String C_FUNDACCO;

    private String C_AGENCYNO;

    private String C_NETNO;

    private String C_TRADEACCO;

    private String C_FUNDCODE;

    private String C_SHARETYPE;

    private String F_CONFIRMBALANCE;

    private String F_CONFIRMSHARES;

    private String F_TRADEFARE;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getC_CUSTNO() {
        return C_CUSTNO;
    }

    public void setC_CUSTNO(String c_CUSTNO) {
        C_CUSTNO = c_CUSTNO;
    }

    public String getC_BUSINFLAG() {
        return C_BUSINFLAG;
    }

    public void setC_BUSINFLAG(String c_BUSINFLAG) {
        C_BUSINFLAG = c_BUSINFLAG;
    }

    public String getD_CDATE() {
        return D_CDATE;
    }

    public void setD_CDATE(String d_CDATE) {
        D_CDATE = d_CDATE;
    }

    public String getC_CSERIALNO() {
        return C_CSERIALNO;
    }

    public void setC_CSERIALNO(String c_CSERIALNO) {
        C_CSERIALNO = c_CSERIALNO;
    }

    public String getD_REQUESTDATE() {
        return D_REQUESTDATE;
    }

    public void setD_REQUESTDATE(String d_REQUESTDATE) {
        D_REQUESTDATE = d_REQUESTDATE;
    }

    public String getD_REQUESTTIME() {
        return D_REQUESTTIME;
    }

    public void setD_REQUESTTIME(String d_REQUESTTIME) {
        D_REQUESTTIME = d_REQUESTTIME;
    }

    public String getC_FUNDACCO() {
        return C_FUNDACCO;
    }

    public void setC_FUNDACCO(String c_FUNDACCO) {
        C_FUNDACCO = c_FUNDACCO;
    }

    public String getC_AGENCYNO() {
        return C_AGENCYNO;
    }

    public void setC_AGENCYNO(String c_AGENCYNO) {
        C_AGENCYNO = c_AGENCYNO;
    }

    public String getC_NETNO() {
        return C_NETNO;
    }

    public void setC_NETNO(String c_NETNO) {
        C_NETNO = c_NETNO;
    }

    public String getC_TRADEACCO() {
        return C_TRADEACCO;
    }

    public void setC_TRADEACCO(String c_TRADEACCO) {
        C_TRADEACCO = c_TRADEACCO;
    }

    public String getC_FUNDCODE() {
        return C_FUNDCODE;
    }

    public void setC_FUNDCODE(String c_FUNDCODE) {
        C_FUNDCODE = c_FUNDCODE;
    }

    public String getC_SHARETYPE() {
        return C_SHARETYPE;
    }

    public void setC_SHARETYPE(String c_SHARETYPE) {
        C_SHARETYPE = c_SHARETYPE;
    }

    public String getF_CONFIRMBALANCE() {
        return F_CONFIRMBALANCE;
    }

    public void setF_CONFIRMBALANCE(String f_CONFIRMBALANCE) {
        F_CONFIRMBALANCE = f_CONFIRMBALANCE;
    }

    public String getF_CONFIRMSHARES() {
        return F_CONFIRMSHARES;
    }

    public void setF_CONFIRMSHARES(String f_CONFIRMSHARES) {
        F_CONFIRMSHARES = f_CONFIRMSHARES;
    }

    public String getF_TRADEFARE() {
        return F_TRADEFARE;
    }

    public void setF_TRADEFARE(String f_TRADEFARE) {
        F_TRADEFARE = f_TRADEFARE;
    }
}
