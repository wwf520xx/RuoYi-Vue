package com.ruoyi.system.domain.reporting;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.utils.bean.ReportBaseEntity;

/**
 * Description: TODO
 *
 * @Author: WANGWENFANG
 * @Date: 2021/11/3 15:27
 */
//public class OtReportingTconfirm extends ReportBaseEntity {

public class OtReportingTconfirm extends BaseEntity {
    private static final long serialVersionUID = 1L;

    @Excel(name = "XXXX")
    private String XXXX;

    @Excel(name = "OOOO")
    private String OOOO;

    @Excel(name = "客户编号")
    private String c_CUSTNO;

    @Excel(name = "业务类型")
    private String c_BUSINFLAG;

    @Excel(name = "确认日期")
    private String d_CDATE;

    @Excel(name = "确认编号")
    private String d_CSERIALNO;

    @Excel(name = "申请日期")
    private String d_REQUESTDATE;

    @Excel(name = "申请时间")
    private String D_REQUESTTIME;

    @Excel(name = "基金账号")
    private String c_FUNDACCO;

    @Excel(name = "销售商")
    private String c_AGENCYNO;

    @Excel(name = "网点")
    private String c_NETNO;

    @Excel(name = "交易账号")
    private String c_TRADEACCO;

    @Excel(name = "基金名称")
    private String c_FUNDCODE;

    @Excel(name = "份额类别")
    private String c_SHARETYPE;

    @Excel(name = "确认金额")
    private String f_CONFIRMBALANCE;

    @Excel(name = "确认份额")
    private String f_CONFIRMSHARES;

    @Excel(name = "交易费")
    private String f_TRADEFARE;

    public String getXXXX() {
        return XXXX;
    }

    public void setXXXX(String XXXX) {
        this.XXXX = XXXX;
    }

    public String getOOOO() {
        return OOOO;
    }

    public void setOOOO(String OOOO) {
        this.OOOO = OOOO;
    }

    public String getC_CUSTNO() {
        return c_CUSTNO;
    }

    public void setC_CUSTNO(String c_CUSTNO) {
        this.c_CUSTNO = c_CUSTNO;
    }

    public String getC_BUSINFLAG() {
        return c_BUSINFLAG;
    }

    public void setC_BUSINFLAG(String c_BUSINFLAG) {
        this.c_BUSINFLAG = c_BUSINFLAG;
    }

    public String getD_CDATE() {
        return d_CDATE;
    }

    public void setD_CDATE(String d_CDATE) {
        this.d_CDATE = d_CDATE;
    }

    public String getD_CSERIALNO() {
        return d_CSERIALNO;
    }

    public void setD_CSERIALNO(String d_CSERIALNO) {
        this.d_CSERIALNO = d_CSERIALNO;
    }

    public String getD_REQUESTDATE() {
        return d_REQUESTDATE;
    }

    public void setD_REQUESTDATE(String d_REQUESTDATE) {
        this.d_REQUESTDATE = d_REQUESTDATE;
    }

    public String getD_REQUESTTIME() {
        return D_REQUESTTIME;
    }

    public void setD_REQUESTTIME(String d_REQUESTTIME) {
        D_REQUESTTIME = d_REQUESTTIME;
    }

    public String getC_FUNDACCO() {
        return c_FUNDACCO;
    }

    public void setC_FUNDACCO(String c_FUNDACCO) {
        this.c_FUNDACCO = c_FUNDACCO;
    }

    public String getC_AGENCYNO() {
        return c_AGENCYNO;
    }

    public void setC_AGENCYNO(String c_AGENCYNO) {
        this.c_AGENCYNO = c_AGENCYNO;
    }

    public String getC_NETNO() {
        return c_NETNO;
    }

    public void setC_NETNO(String c_NETNO) {
        this.c_NETNO = c_NETNO;
    }

    public String getC_TRADEACCO() {
        return c_TRADEACCO;
    }

    public void setC_TRADEACCO(String c_TRADEACCO) {
        this.c_TRADEACCO = c_TRADEACCO;
    }

    public String getC_FUNDCODE() {
        return c_FUNDCODE;
    }

    public void setC_FUNDCODE(String c_FUNDCODE) {
        this.c_FUNDCODE = c_FUNDCODE;
    }

    public String getC_SHARETYPE() {
        return c_SHARETYPE;
    }

    public void setC_SHARETYPE(String c_SHARETYPE) {
        this.c_SHARETYPE = c_SHARETYPE;
    }

    public String getF_CONFIRMBALANCE() {
        return f_CONFIRMBALANCE;
    }

    public void setF_CONFIRMBALANCE(String f_CONFIRMBALANCE) {
        this.f_CONFIRMBALANCE = f_CONFIRMBALANCE;
    }

    public String getF_CONFIRMSHARES() {
        return f_CONFIRMSHARES;
    }

    public void setF_CONFIRMSHARES(String f_CONFIRMSHARES) {
        this.f_CONFIRMSHARES = f_CONFIRMSHARES;
    }

    public String getF_TRADEFARE() {
        return f_TRADEFARE;
    }

    public void setF_TRADEFARE(String f_TRADEFARE) {
        this.f_TRADEFARE = f_TRADEFARE;
    }
}
