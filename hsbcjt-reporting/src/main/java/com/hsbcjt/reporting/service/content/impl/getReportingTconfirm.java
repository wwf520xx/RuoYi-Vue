package com.hsbcjt.reporting.service.content.impl;

import com.hsbcjt.reporting.domain.dto.ReportingContent;
import com.hsbcjt.reporting.service.content.IGetReportingContent;
import com.ruoyi.system.domain.reporting.ReportingTconfirm;
import com.ruoyi.system.domain.vo.RequestReportParamsVo;
import com.ruoyi.system.mapper.ReportingTconfirmMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Description: TODO
 *
 * @Author: WANGWENFANG
 * @Date: 2021/11/3 17:21
 */
@Component
public class getReportingTconfirm implements IGetReportingContent {

    @Override
    public ReportingContent selectReportingTconfirmList(RequestReportParamsVo requestReportParamsVo) {
        List<ReportingTconfirm> reportingTconfirms = requestReportParamsVo.getReportingTconfirmMapper().selectReportingTconfirmList(requestReportParamsVo);
        ReportingContent<ReportingTconfirm> reportingContent = new ReportingContent<ReportingTconfirm>();
        reportingContent.setList(reportingTconfirms);
        return reportingContent;
    }

}
