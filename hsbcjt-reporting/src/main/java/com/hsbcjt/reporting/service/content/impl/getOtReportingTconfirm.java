package com.hsbcjt.reporting.service.content.impl;

import com.hsbcjt.reporting.domain.dto.ReportingContent;
import com.hsbcjt.reporting.service.content.IGetReportingContent;
import com.ruoyi.system.domain.reporting.OtReportingTconfirm;
import com.ruoyi.system.domain.vo.RequestReportParamsVo;

import java.util.ArrayList;
import java.util.List;

/**
 * Description: TODO
 *
 * @Author: WANGWENFANG
 * @Date: 2021/11/3 17:21
 */
public class getOtReportingTconfirm implements IGetReportingContent {

    @Override
    public ReportingContent selectReportingTconfirmList(RequestReportParamsVo requestReportParamsVo) {
        OtReportingTconfirm otReportingTconfirm = new OtReportingTconfirm();
        otReportingTconfirm.setC_CUSTNO("333333333333");
        List<OtReportingTconfirm> otReportingTconfirmS= new ArrayList<OtReportingTconfirm>();
        otReportingTconfirmS.add(otReportingTconfirm);
        ReportingContent<OtReportingTconfirm> reportingContent = new ReportingContent<OtReportingTconfirm>();
        reportingContent.setList(otReportingTconfirmS);
        return reportingContent;
    }
    
}
