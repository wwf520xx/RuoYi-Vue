package com.hsbcjt.reporting.service.content;

import com.hsbcjt.reporting.domain.dto.ReportingContent;
import com.ruoyi.system.domain.vo.RequestReportParamsVo;
import org.apache.poi.ss.formula.functions.T;

/**
 * Description: TODO
 *
 * @Author: WANGWENFANG
 * @Date: 2021/11/3 17:19
 */
public interface IGetReportingContent {

    public ReportingContent<T> selectReportingTconfirmList(RequestReportParamsVo requestReportParamsVo);

}
