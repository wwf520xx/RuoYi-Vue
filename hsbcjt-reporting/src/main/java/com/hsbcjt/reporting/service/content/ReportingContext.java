package com.hsbcjt.reporting.service.content;

import com.hsbcjt.reporting.domain.dto.ReportingContent;
import com.ruoyi.system.domain.vo.RequestReportParamsVo;

/**
 * Description: TODO
 *
 * @Author: WANGWENFANG
 * @Date: 2021/11/3 16:47
 */
public class ReportingContext {

    private IGetReportingContent getReportingContent;

    public ReportingContext(IGetReportingContent getReportingContent) {
        this.getReportingContent = getReportingContent;
    }

    public ReportingContent getReportingContent(RequestReportParamsVo requestReportParamsVo){
        return getReportingContent.selectReportingTconfirmList(requestReportParamsVo);
    }

}
