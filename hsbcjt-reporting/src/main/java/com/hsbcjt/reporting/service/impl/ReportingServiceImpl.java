package com.hsbcjt.reporting.service.impl;

import com.hsbcjt.reporting.domain.dto.ReportingContent;
import com.hsbcjt.reporting.enums.ReportingBeans;
import com.hsbcjt.reporting.enums.ReportingEntity;
import com.hsbcjt.reporting.service.content.IGetReportingContent;
import com.hsbcjt.reporting.service.content.ReportingContext;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.utils.bean.ReportBaseEntity;
import com.ruoyi.common.utils.html.TableUtil;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.reporting.ReportingTconfirm;
import com.ruoyi.system.domain.vo.RequestReportParamsVo;
import com.hsbcjt.reporting.service.IReportingService;
import com.ruoyi.system.mapper.ReportingTconfirmMapper;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Description: TODO
 *
 * @Author: WANGWENFANG
 * @Date: 2021/10/26 13:38
 */
@Service
public class ReportingServiceImpl implements IReportingService {

    @Autowired
    private ReportingTconfirmMapper reportingTconfirmMapper;

    @Override
    public AjaxResult selectReportHeader(RequestReportParamsVo requestReportParamsVo) {
        String tableClassName = ReportingEntity.getReportingCode(requestReportParamsVo.getTableName());
        Class tClz = null;
        try {
            tClz = Class.forName(tableClassName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        TableUtil<BaseEntity> util = new TableUtil<BaseEntity>(tClz);
        return AjaxResult.success(util.getFieldVos());
    }

    @Override
    public AjaxResult selectReportingData(RequestReportParamsVo requestReportParamsVo) {
        requestReportParamsVo.setReportingTconfirmMapper(reportingTconfirmMapper);
        String className = ReportingBeans.getReportingCode(requestReportParamsVo.getTableName());
        Class clz = null;
        Object obj = null;
        try {
            clz = Class.forName(className);
            obj = clz.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        ReportingContext reportingContext = new ReportingContext((IGetReportingContent) obj);
        ReportingContent reportingContent = reportingContext.getReportingContent(requestReportParamsVo);
        return AjaxResult.success(reportingContent.getList());
    }

    @Override
    public AjaxResult reportingExport(RequestReportParamsVo requestReportParamsVo) {
        requestReportParamsVo.setReportingTconfirmMapper(reportingTconfirmMapper);
        String className = ReportingBeans.getReportingCode(requestReportParamsVo.getTableName());
        Class clz = null;
        Object obj = null;
        try {
            clz = Class.forName(className);
            obj = clz.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        ReportingContext reportingContext = new ReportingContext((IGetReportingContent) obj);
        ReportingContent reportingContent = reportingContext.getReportingContent(requestReportParamsVo);

        String tableClassName = ReportingEntity.getReportingCode(requestReportParamsVo.getTableName());
        Class tClz = null;
        try {
            tClz = Class.forName(tableClassName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ExcelUtil<BaseEntity> util = new ExcelUtil<BaseEntity>(tClz);
        return util.exportExcel(reportingContent.getList(), requestReportParamsVo.getTableName());
    }

//    @Autowired
//    private ReportingTconfirmMapper reportingTconfirmMapper;

//    @Override
//    public ReportingContent selectReportingTconfirmList(RequestReportParamsVo requestReportParamsVo) {
//        List<ReportingTconfirm> reportingTconfirms = reportingTconfirmMapper.selectReportingTconfirmList(requestReportParamsVo);
//        ReportingContent<ReportingTconfirm> reportingContent = new ReportingContent<ReportingTconfirm>();
//        reportingContent.setList(reportingTconfirms);
//        return reportingContent;
//    }


}
