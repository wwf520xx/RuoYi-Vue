package com.hsbcjt.reporting.service;

import com.hsbcjt.reporting.domain.dto.ReportingContent;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.domain.vo.RequestReportParamsVo;
import org.apache.poi.ss.formula.functions.T;

import java.util.List;

/**
 * Description: TODO
 *
 * @Author: WANGWENFANG
 * @Date: 2021/10/26 13:36
 */
public interface IReportingService {

    /**
     * 查询报列表
     *
     * @param requestReportParamsVo 报表
     * @return 报表集合
     */
//    public ReportingContent<T> selectReportingTconfirmList(RequestReportParamsVo requestReportParamsVo);

     public AjaxResult selectReportHeader(RequestReportParamsVo requestReportParamsVo);

     public AjaxResult selectReportingData(RequestReportParamsVo requestReportParamsVo);

     public AjaxResult reportingExport(RequestReportParamsVo requestReportParamsVo);

}
