package com.hsbcjt.reporting.domain.dto;

import java.util.List;

/**
 * Description: TODO
 *
 * @Author: WANGWENFANG
 * @Date: 2021/11/3 16:07
 */
public class ReportingContent<T> {

    /**
     * 导入导出数据列表
     */
    private List<T> list;

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}
