package com.hsbcjt.reporting.enums;

/**
 * Description: TODO
 *
 * @Author: WANGWENFANG
 * @Date: 2021/11/4 9:30
 */
public enum ReportingEntity {


    reportingTconfirm("reportingTconfirm", "com.ruoyi.system.domain.reporting.ReportingTconfirm"),

    otReportingTconfirm("otReportingTconfirm", "com.ruoyi.system.domain.reporting.OtReportingTconfirm");

    private String reportingCode;

    private String className;

    private ReportingEntity(String reportingCode, String className) {
        this.reportingCode = reportingCode;
        this.className = className;
    }

    public String getReportingCode() {
        return reportingCode;
    }

    public void setReportingCode(String reportingCode) {
        this.reportingCode = reportingCode;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    /**
     * @param reportingCode
     * @return
     */
    public static String getReportingCode(String reportingCode) {
        for (ReportingEntity e : ReportingEntity.values()) {
            if (reportingCode.equals(e.getReportingCode())) {
                return e.getClassName();
            }
        }
        return "";
    }

}
