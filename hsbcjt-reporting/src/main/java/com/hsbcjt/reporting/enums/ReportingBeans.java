package com.hsbcjt.reporting.enums;

/**
 * Description: TODO
 *
 * @Author: WANGWENFANG
 * @Date: 2021/11/2 18:05
 */
public enum ReportingBeans {

    reportingTconfirm("reportingTconfirm", "com.hsbcjt.reporting.service.content.impl.getReportingTconfirm"),

    otReportingTconfirm("otReportingTconfirm", "com.hsbcjt.reporting.service.content.impl.getOtReportingTconfirm");

    private String reportingCode;

    private String className;

    private ReportingBeans(String reportingCode, String className) {
        this.reportingCode = reportingCode;
        this.className = className;
    }

    public String getReportingCode() {
        return reportingCode;
    }

    public void setReportingCode(String reportingCode) {
        this.reportingCode = reportingCode;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    /**
     * @param reportingCode
     * @return
     */
    public static String getReportingCode(String reportingCode) {
        for (ReportingBeans e : ReportingBeans.values()) {
            if (reportingCode.equals(e.getReportingCode())) {
                return e.getClassName();
            }
        }
        return "";
    }


}
