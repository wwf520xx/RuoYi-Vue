import request from '@/utils/request'

// 查询报列表
export function listReporting(query) {
  return request({
    url: '/system/reporting/list',
    method: 'get',
    params: query
  })
}

// 查询报详细
export function getReporting(reportingId) {
  return request({
    url: '/system/reporting/' + reportingId,
    method: 'get'
  })
}

// 新增报
export function addReporting(data) {
  return request({
    url: '/system/reporting',
    method: 'post',
    data: data
  })
}

// 修改报
export function updateReporting(data) {
  return request({
    url: '/system/reporting',
    method: 'put',
    data: data
  })
}

// 删除报表
export function delReporting(reportingId) {
  return request({
    url: '/system/reporting/' + reportingId,
    method: 'delete'
  })
}

// 导出报
export function exportReporting(query) {
  return request({
    url: '/reporting/export',
    method: 'get',
    params: query
  })
}

// 查询报列表
export function getReportingList(query) {
  return request({
    url: '/reporting/list',
    method: 'get',
    params: query
  })
}

// 查询报表下拉树结构
export function treeselect() {
  return request({
    url: '/system/reporting/treeselect',
    method: 'get'
  })
}

// 根据角色ID查询报表下拉树结构
export function roleReportingTreeselect(roleId) {
  return request({
    url: '/system/reporting/roleReportingTreeselect/' + roleId,
    method: 'get'
  })
}

// 查询报表头
export function selectReportHeader(query) {
  return request({
    url: '/reporting/selectReportHeader',
    method: 'get',
    params: query
  })
}
