package com.ruoyi.web.controller.reporting;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.reporting.ReportingTconfirm;
import com.ruoyi.system.domain.vo.RequestReportParamsVo;
import com.hsbcjt.reporting.service.IReportingService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 报Controller
 *
 * @author wangwf
 *
 * @date 2021-10-20
 */
@RestController
@RequestMapping("/reporting")
public class ReportingController extends BaseController
{
    @Autowired
    private IReportingService reportingService;

    /**
     * 获取菜单列表
     */
    @GetMapping("/selectReportHeader")
    public AjaxResult selectReportHeader(RequestReportParamsVo requestReportParamsVo) {
        return reportingService.selectReportHeader(requestReportParamsVo);
    }

    /**
     * 获取菜单列表
     */
    @PreAuthorize("@ss.hasPermi('system:menu:list')")
    @GetMapping("/list")
    public AjaxResult list(RequestReportParamsVo requestReportParamsVo) {
        return reportingService.selectReportingData(requestReportParamsVo);
    }

    /**
     * 导出报列表
     */
    @PreAuthorize("@ss.hasPermi('system:reporting:export')")
    @Log(title = "报表管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(RequestReportParamsVo requestReportParamsVo)
    {
//        List<ReportingTconfirm> list = reportingService.selectReportingTconfirmList(reportingTconfirm);
//        ExcelUtil<ReportingTconfirm> util = new ExcelUtil<ReportingTconfirm>(ReportingTconfirm.class);
//        return util.exportExcel(list, "测试表");
        return reportingService.reportingExport(requestReportParamsVo);
    }

//    @PreAuthorize("@ss.hasPermi('system:reporting:export')")
//    @Log(title = "报表管理", businessType = BusinessType.EXPORT)
//    @GetMapping("/export")
//    public AjaxResult export(RequestReportParamsVo reportParamsVo)
//    {
//        List<ReportingTconfirm> list = reportingService.selectReportingTconfirmList(reportingTconfirm);
//        ExcelUtil<ReportingTconfirm> util = new ExcelUtil<ReportingTconfirm>(ReportingTconfirm.class);
//        return util.exportExcel(list, "测试表");
//    }

}
