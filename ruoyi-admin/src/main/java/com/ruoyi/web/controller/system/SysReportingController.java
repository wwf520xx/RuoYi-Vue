package com.ruoyi.web.controller.system;

import java.util.List;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.entity.SysReporting;
import com.ruoyi.common.utils.bean.ReportBaseEntity;
import com.ruoyi.common.utils.html.TableUtil;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.service.ISysReportingService;
import com.ruoyi.common.utils.poi.ExcelUtil;

/**
 * 报Controller
 *
 * @author wangwf
 *
 * @date 2021-10-20
 */
@RestController
@RequestMapping("/system/reporting")
public class SysReportingController extends BaseController
{
    @Autowired
    private ISysReportingService reportingService;

    /**
     * 查询报列表
     */
    @PreAuthorize("@ss.hasPermi('system:reporting:list')")
    @GetMapping("/list")
    public AjaxResult list(SysReporting reporting)
    {
        List<SysReporting> reportings = reportingService.selectReportingList(reporting, getUserId());
//        TableUtil<SysReporting> util = new TableUtil<SysReporting>(SysReporting.class);
//        return AjaxResult.success(util.getFieldVos(),reportings);
        return AjaxResult.success(reportings);
    }

    /**
     * 导出报列表
     */
    @PreAuthorize("@ss.hasPermi('system:reporting:export')")
    @Log(title = "报表管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysReporting sysReporting)
    {
        List<SysReporting> list = reportingService.selectReportingList(sysReporting, getUserId());
        ExcelUtil<SysReporting> util = new ExcelUtil<SysReporting>(SysReporting.class);
        return util.exportExcel(list, sysReporting.getReportingName());
    }

    /**
     * 获取报详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:reporting:query')")
    @GetMapping(value = "/{reportingId}")
    public AjaxResult getInfo(@PathVariable("reportingId") Long reportingId)
    {
        return AjaxResult.success(reportingService.selectReportingByReportingId(reportingId));
    }

    /**
     * 新增报
     */
    @PreAuthorize("@ss.hasPermi('system:reporting:add')")
    @Log(title = "报表管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody SysReporting reporting)
    {
        if (UserConstants.NOT_UNIQUE.equals(reportingService.checkReportingNameUnique(reporting)))
        {
            return AjaxResult.error("新增报表'" + reporting.getReportingName() + "'失败，报表名称已存在");
        }
        else if (UserConstants.NOT_UNIQUE.equals(reportingService.checkReportingCodeUnique(reporting)))
        {
            return AjaxResult.error("新增报表'" + reporting.getReportingName() + "'失败，报表编码已存在");
        }
        reporting.setCreateBy(getUsername());
        return toAjax(reportingService.insertReporting(reporting));
    }

    /**
     * 修改报表
     */
    @PreAuthorize("@ss.hasPermi('system:reporting:edit')")
    @Log(title = "报表管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody SysReporting reporting)
    {
        if (UserConstants.NOT_UNIQUE.equals(reportingService.checkReportingNameUnique(reporting)))
        {
            return AjaxResult.error("修改报表'" + reporting.getReportingName() + "'失败，报表名称已存在");
        }
        else if (UserConstants.NOT_UNIQUE.equals(reportingService.checkReportingCodeUnique(reporting)))
        {
            return AjaxResult.error("修改报表'" + reporting.getReportingName() + "'失败，报表编码已存在");
        }
        else if (reporting.getReportingId().equals(reporting.getParentId()))
        {
            return AjaxResult.error("修改报表'" + reporting.getReportingName() + "'失败，上级菜单不能选择自己");
        }
        reporting.setUpdateBy(getUsername());
        return toAjax(reportingService.updateReporting(reporting));
    }

    /**
     * 删除报
     */
    @PreAuthorize("@ss.hasPermi('system:reporting:remove')")
    @Log(title = "报表管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{reportingId}")
    public AjaxResult remove(@PathVariable Long reportingId)
    {
        if (reportingService.hasChildByReportingId(reportingId))
        {
            return AjaxResult.error("存在子节点,不允许删除");
        }
        if (reportingService.checkReportingExistRole(reportingId))
        {
            return AjaxResult.error("报表已分配,不允许删除");
        }
        return toAjax(reportingService.deleteReportingByReportingId(reportingId));
    }


    /**
     * 获取菜单下拉树列表
     */
    @GetMapping("/treeselect")
    public AjaxResult treeselect(SysReporting reporting)
    {
        List<SysReporting> reportings = reportingService.selectReportingList(reporting, getUserId());
        return AjaxResult.success(reportingService.buildReportingTreeSelect(reportings));
    }

    /**
     * 加载对应角色菜单列表树
     */
    @GetMapping(value = "/roleReportingTreeselect/{roleId}")
    public AjaxResult roleReportingTreeselect(@PathVariable("roleId") Long roleId)
    {
        List<SysReporting> reportings = reportingService.selectReportingList(getUserId());
        AjaxResult ajax = AjaxResult.success();
        ajax.put("checkedKeys", reportingService.selectReportingListByRoleId(roleId));
        ajax.put("reportings", reportingService.buildReportingTreeSelect(reportings));
        return ajax;
    }

}
